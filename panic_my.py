target_phrase = 'on tap'
tlist = list(target_phrase)

phrase = "Don`t panic!"
plist = list(phrase)

print(phrase)
print(plist)

plist.remove(' ')

for k in range(4):
    plist.pop()

for char in plist:
    if char not in tlist:
        plist.remove(char)

plist.insert(2,' ')

plist.insert(4, plist.pop())

new_phrase = ''.join(plist)
print(plist)
print(new_phrase)
