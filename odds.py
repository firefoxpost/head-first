from datetime import datetime

today = datetime.today()

right_this_minute = today.minute

if right_this_minute % 2 != 0:
    print(right_this_minute, "This minute seems a little odd.")
else:
    print(right_this_minute, "Not an odd minute")
