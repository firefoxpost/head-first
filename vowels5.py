vowels = ['a', 'e', 'i', 'o', 'u']
word = input('Provide a word to search for vowels: ')
found = {}

for letter in word:
    letter = letter.lower()
    if letter in vowels:
        if letter in found and found[letter] != 0:
            found[letter] += 1
        else:
            found[letter] = 0

for k, v in sorted(found.items()):
    print(k, 'was found', v, 'time(s).')
