# генератор списков

data = [1,2,3,4,5,6,7,8]

evens = [num for num in data if not num % 2]

print(evens)


# генератор словарей

hash = {11: 'test1', 22 : 'test2', 33 : 'test3', 44 : 'test4', 222 : 'test2', 444 : 'test4', 333 : 'test3'}

hash_values = {val: [k for k,v in hash.items() if v == val] for val in set(hash.values())}

print(hash_values)


# генератор множеств

vowels = ['a', 'e', 'i', 'o', 'u']

message = "Don't forget to pack your towel."

found = { v  for v in vowels if v in message }

print(found)
