from functools import wraps

def my_decorator(func : object) -> object:
    @wraps(func)
    def wrapper(*args, **kwargs) -> object:
        print('Now new action is added!')
        return func(*args, **kwargs)

    return wrapper



@my_decorator
def test_decor(text : 'string') -> 'string':
    print(text)

test_decor('This is the test function!')
