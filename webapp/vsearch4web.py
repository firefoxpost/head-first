from flask import Flask, render_template, request, escape, session
from vsearch import search4letters
from DBcm import UseDatabase, ConnectionError, CridentialsError, SQLError
from checker import check_logged_in
from threading import Thread
from time import sleep
import json


app = Flask(__name__)

app.secret_key = 'VeryVeryVeryLongAndSecretKey'

with open('config/db.json') as config:
    app.config['dbconfig'] = json.load(config)


@app.route('/search4', methods=['POST'])
def do_search() -> 'html':

    @copy_current_request_context
    def log_request(req: 'flask_request', res : str) -> None:
        sleep(15)
        with UseDatabase(app.config['dbconfig']) as cursor:
            _SQL = """insert into log
                     (phrase, letters, ip, browser_string, results)
                     values
                     (%s, %s, %s, %s, %s)"""

            cursor.execute(_SQL, (req.form['phrase'],
                                  req.form['letters'],
                                  req.remote_addr,
                                  req.user_agent.browser,
                                  res, ))

    phrase = request.form['phrase']
    letters = request.form['letters']
    title = 'Here are your results:'
    result = str(search4letters(phrase, letters))
    try:
        t = Thread(target=log_request, args=(request, result))
        t.start()
    except Exception as err:
        print("*****Logging failed with this error:", str(err))

    return render_template('results.html',
                            the_phrase=phrase,
                            the_letters=letters,
                            the_title=title,
                            the_result=result)


@app.route('/')
@app.route('/entry')
def entry_page() -> 'html':
    return render_template('entry.html', the_title='Welcome to search4letters on web!')


@app.route('/viewlog')
@check_logged_in
def view_log() -> 'html':
    contents = []
    try:
        with UseDatabase(app.config['dbconfig']) as cursor:
            _SQL = """select id, phrase, letters, ip, browser_string, results
                from log"""
            cursor.execute(_SQL)
            contents = cursor.fetchall()

        titles = ('ID', 'Phrase', 'Letters', 'Remote address', 'User Agent', 'Results')
        return render_template('viewlog.html', the_title='View log', the_row_titles=titles, the_data=contents)

    except ConnectionError as err:
        print('Is your database switched on? Error:', str(err))
    except CridentialsError as err:
        print('UserId/Password issues. Error:', str(err))
    except SQLError as err:
        print('Is your query correct?. Error:', str(err))
    except Exception as err:
        print('Something went wrong:', str(err))
    return 'Error'

@app.route('/login')
def do_login() -> str:
    session['logged_in'] = True
    return 'You are logged in now!'

@app.route('/logout')
def do_logout() -> str:
    session.pop('logged_in')
    return 'You are logged out!'

if __name__ == '__main__':
    app.run(debug=True)
