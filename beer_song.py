word = 'bottles'
word_wall = 'of beer on the wall.'

for beer_num in range(99, 0, -1):
    print(beer_num, word, word_wall )
    print(beer_num, word, 'of beer')
    print('Take one down.')
    print('Pass it around.')
    if beer_num == 1:
        print('No more', word, word_wall)
    else:
        new_num = beer_num - 1
        if new_num == 1:
            word = 'bottle'
        print(new_num, word, word_wall)
    print()
