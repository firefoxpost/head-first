def search4vowels(phrase: str) -> set:
    """Выводит гласные, найденные во введенном слове."""
    return set('aeiou').intersection(set(phrase))


def search4letters(phrase: str, letters: str = 'aeiou') -> set:
    """Выводит гласные из 'letters', найденные во фразе 'phrase'."""
    return set(letters).intersection(set(phrase))
