from datetime import datetime
import time
import random

today = datetime.today()

for i in range(5):
    right_this_minute = today.minute

    if right_this_minute % 2 != 0:
        print(right_this_minute, "This minute seems a little odd.")
    else:
        print(right_this_minute, "Not an odd minute")
    wait_time = random.randint(1, 60)
    time.sleep(wait_time)
